<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemilih extends Model
{
    use HasFactory;
    
    protected $fillable = ['nis_pemilih', 'nama_pemilih', 'kelas_pemilih', 'jk_pemilih','no_token'];
}
