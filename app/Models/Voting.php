<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    use HasFactory;

    protected $fillable = ['no_urut', 'nis'];
    protected $primaryKey = ["id_voting"];
    public $incrementing = false;
    protected $keyType= "string";
}
