<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Kandidat extends Model
{
    use HasFactory;

    protected $fillable = ['no_kandidat', 'nis_kandidat', 'nama_kandidat', 'tempat_lahir', 'tanggal_lahir', 'kelas_kandidat', 'jk_kandidat',  'telp_kandidat', 'alamat_kandidat', 'pengalaman_kandidat', 'visi', 'misi', 'foto'];
    protected $primaryKey = ["id_kandidat"];
    public $incrementing = false;
    protected $keyType= "string";
}
