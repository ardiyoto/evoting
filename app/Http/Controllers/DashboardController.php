<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pemilih;
use App\Models\Kandidat;

class DashboardController extends Controller
{
    function index(){
        $kandidat = Kandidat::All()->count();
        $pemilih = Pemilih::All()->count();
        $voted = Pemilih::where('status',1)->get()->count();
        $notvoted = Pemilih::where('status',0)->get()->count();
        return view('admin.dashboard',compact('kandidat','pemilih','voted','notvoted'));
    }
}
