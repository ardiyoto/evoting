<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kandidat;
use Illuminate\Support\Facades\DB;


class KandidatController extends Controller
{
    function index(){
        $title = "Kandidat";
        $data = Kandidat::All();
 
        return view('admin.kandidat', compact('title', 'data'));
    }

    function form(Request $req){
        $title = $req->id == "" ? "Form Kandidat" : "Edit Kandidat";
        $data = Kandidat::where('id_kandidat', $req->id)->first();     
        
        return view('admin/form-kandidat', compact('title', 'data'));
    }


    function save(Request $req){
        // VALIDATION
        $req->validate(
            [
                'no_kandidat' => 'required|unique:kandidats,no_kandidat,'.$req->input('id_kandidat').',id_kandidat',
                'nis_kandidat' => 'required|unique:kandidats,nis_kandidat,'.$req->input('id_kandidat').',id_kandidat',
                'foto' => 'mimes:jpeg,png,gif,jpg,svg|max:2048'
            ],
            [
                'no_kandidat.required' => 'Nomor Kandidat Tidak Boleh Kosong !',
                'no_kandidat.unique' => 'Nomor Kandidat Sudah Digunakan !',
                'nis_kandidat.required' => 'NIS Tidak Boleh Kosong !',
                'nis_kandidat.unique' => 'NIS Sudah Digunakan!',
            ]
        );
        try{
              // File
              if($req->foto){
                $filename="Kandidat.".$req->input('no_kandidat').".".$req->foto->extension();
                $req->foto->move(public_path('assets/images'),$filename);
            } else {
                // No Fie
                $filename = $req->input('id_kandidat') != "" ? $req->input('old_foto') : "";
            }

            if(!$req->input('id_kandidat')):
                // INSERT
                Kandidat::create([
                    "no_kandidat" => $req->input('no_kandidat'),
                    "nis_kandidat" => $req->input('nis_kandidat'),
                    "nama_kandidat" => $req->input('nama_kandidat'),
                    "tempat_lahir" => $req->input('tempat_lahir'),
                    "tanggal_lahir" => $req->input('tanggal_lahir'),
                    "kelas_kandidat" => $req->input('kelas_kandidat'),
                    "jk_kandidat" => $req->input('jk_kandidat'),
                    "telp_kandidat" => $req->input('telp_kandidat'),
                    "alamat_kandidat" => $req->input('alamat_kandidat'),
                    "pengalaman_kandidat" => $req->input('pengalaman_kandidat'),
                    "visi" => $req->input('visi'),
                    "misi" => $req->input('misi'),
                    "foto" => $filename,
                ]);
            else:
                // UPDATE
                Kandidat::where('id_kandidat', $req->input('id_kandidat'))->update([
                    "no_kandidat" => $req->input('no_kandidat'),
                    "nis_kandidat" => $req->input('nis_kandidat'),
                    "nama_kandidat" => $req->input('nama_kandidat'),
                    "tempat_lahir" => $req->input('tempat_lahir'),
                    "tanggal_lahir" => $req->input('tanggal_lahir'),
                    "kelas_kandidat" => $req->input('kelas_kandidat'),
                    "jk_kandidat" => $req->input('jk_kandidat'),
                    "telp_kandidat" => $req->input('telp_kandidat'),
                    "alamat_kandidat" => $req->input('alamat_kandidat'),
                    "pengalaman_kandidat" => $req->input('pengalaman_kandidat'),
                    "visi" => $req->input('visi'),
                    "misi" => $req->input('misi'),
                    "foto" => $filename,
                ]);
            endif;
            
            return redirect('admin/kandidat')->with(['type'=>'success','message'=>'Data Berhasil Disimpan !']);
        } catch(\Exception $err){
            return redirect('admin/kandidat')->with(['type'=>'danger','message'=>'Terjadi Kesalahan !']);
        }
    }

    function delete(Request $req){
        try{
            Kandidat::where('id_kandidat',$req->id)->delete();
            return redirect('admin/kandidat')->with(['type'=>'success','message'=>'Data Berhasil Dihapus !']);
        } catch(\Exception $err){
            return redirect('admin/kandidat')->with(['type'=>'danger','message'=>'Terjadi Kesalahan !']);
        }
    }

   
}
