<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Voting;
use App\Models\Pemilih;
use App\Models\Kandidat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class VoteController extends Controller
{
    function login(Request $req){
        $title = "Login";
        //$data = Voting::where('id_voting',$req->id)->first();
        

        return view('loginvote',compact('title'));
    }

    function vote(Request $req){
        $title = "Vote";
        $url = url("/endvoting");
        $data = Kandidat::All();
        $nis = $req->session()->get('nis');
        if($req->session()->get('nis')){
            return view('vote', compact('title', 'data','nis','url'));
        } else {
            return redirect('/');
        }
    }

    function cekvoting(Request $req){
        $data=Pemilih::where(["nis_pemilih"=>$req->input("nis_pemilih"),"no_token"=>$req->input("no_token")])->first();
        if($data){
            if($data->status==0){
                $req->session()->put("nis",$req->input("nis_pemilih"));
                return redirect('voting');
            } else {
                return redirect('thank');
            }
        } else {
            return redirect('/')->with(['type'=>'danger','message'=>'Harap Login Kembali !']);
        }
    }

    function save_voting(Request $req)
    {
        try {
            $save = Voting::create([
                "nis"=>Hash::make($req->session()->get('nis')),
                "no_urut"=>$req->no_urut
            ]);

            if($save){
                Pemilih::where(["nis_pemilih"=>$req->session()->get('nis')])
                ->update([
                    "status"=>1
                ]);
                return redirect('thank');
            }

        } catch(Exception $err){

        }
    }

    function endvoting(Request $req){
        $req->session()->forget('nis');
        return redirect(url("/"));
    }

    function thank(){
        $url = url("/endvoting");
        $title = "Thank You";
        return view('thank',compact('url','title'));
    }
}
