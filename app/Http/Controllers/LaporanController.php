<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pemilih;
use App\Models\Kandidat;
use App\Models\Voting;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\DB;


class LaporanController extends Controller
{
    // public function index(){
    //     $title = 'Laporan';
    //     $data = Kandidat::All();

    //     return view('admin/rpt-laporan',compact('title','data'));
    // }

    function generate_absen(){
        $data = Pemilih::All();
        $content =  view('admin.report_absen',compact('data'));
        $pdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'Folio',
            'orientation' => 'P'
        ]);
        $pdf->WriteHTML($content);
        $pdf->Output(public_path().'/Laporan Absen.pdf','I');
    }

    function berita_acara(){
        //$data = Kandidat::All();
        $data = DB::select(DB::raw('select `kandidats`.*, count(votings.no_urut) as hasil from `kandidats` left join `votings` on `kandidats`.`no_kandidat` = `votings`.`no_urut` group by `kandidats`.`no_kandidat`'));
        $content =  view('admin.berita_acara',compact('data'));
        $pdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'Folio',
            'orientation' => 'P'
        ]);
        $pdf->WriteHTML($content);
        $pdf->Output(public_path().'/Berita Acara.pdf','I');
    }
}
