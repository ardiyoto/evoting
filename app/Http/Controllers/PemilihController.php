<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Pemilih;

class PemilihController extends Controller
{
    function index(){
        $title = "Pemilih";
        $data = Pemilih::All();
 
   
        return view('admin.pemilih', compact('title', 'data'));
    }

    function form(Request $req){
        $title = $req->id == "" ? "Form Pemilih" : "Edit Pemilih";
        $data = Pemilih::where('id_pemilih', $req->id)->first();     
        
        return view('admin/form-pemilih', compact('title', 'data'));
    }

    function save(Request $req){
        try{
            if(!$req->input('id_pemilih')):
                // INSERT
                Pemilih::create([
                    "id_pemilih" => $req->input('id_pemilih'),
                    "nis_pemilih" => $req->input('nis_pemilih'),
                    "nama_pemilih" => $req->input('nama_pemilih'),
                    "kelas_pemilih" => $req->input('kelas_pemilih'),
                    "jk_pemilih" => $req->input('jk_pemilih'),                    
                ]);
            else:
                // UPDATE
               Pemilih::where('id_pemilih', $req->input('id_pemilih'))->update([
                    "id_pemilih" => $req->input('id_pemilih'),
                    "nis_pemilih" => $req->input('nis_pemilih'),
                    "nama_pemilih" => $req->input('nama_pemilih'),
                    "kelas_pemilih" => $req->input('kelas_pemilih'),
                    "jk_pemilih" => $req->input('jk_pemilih')                    
                ]);
            endif;
            return redirect('admin/pemilih')->with(['type'=>'success','message'=>'Data Berhasil Disimpan !']);
        } catch(\Exception $err){
            return redirect('admin/pemilih')->with(['type'=>'danger','message'=>'Terjadi Kesalahan !']);
        }
    }

    function set_token(Request $req){
        try{
            Pemilih::where("id_pemilih",$req->id)->update([
                "no_token"=>strtoupper(Str::random(6)),
            ]);
            return redirect('admin/pemilih')->with(['type'=>'success','message'=>'Nomor Token Berhasil Disetting !']);
        } catch(\Exception $err){
            return redirect('admin/pemilih')->with(['type'=>'danger','message'=>'Nomor Token Berhasil Disetting !']);
        }
    }

    function delete(Request $req){
        try{
           Pemilih::where('id_pemilih',$req->id)->delete();
            return redirect('admin/pemilih')->with(['type'=>'success','message'=>'Data Berhasil Dihapus !']);
        } catch(\Exception $err){
            return redirect('admin/pemilih')->with(['type'=>'danger','message'=>'Terjadi Kesalahan !']);
        }
    }

}
