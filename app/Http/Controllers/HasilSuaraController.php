<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Voting;
use App\Models\Kandidat;
use Illuminate\Support\Facades\DB;

class HasilSuaraController extends Controller
{
    function index(){
        $title = 'Hasil Suara';
        // $data = Kandidat::All();
        // $data = DB::table('kandidats')
        // ->select('kandidats.*', DB::raw("count(votings.no_urut) as count"))
        // ->join('votings','kandidats.no_kandidat','=','votings.no_urut')
        // // ->where(['something' => 'something', 'otherThing' => 'otherThing'])
        // ->groupBy('kandidats.no_kandidat')
        // ->get();
        $data = DB::select(DB::raw('select `kandidats`.*, count(votings.no_urut) as hasil from `kandidats` left join `votings` on `kandidats`.`no_kandidat` = `votings`.`no_urut` group by `kandidats`.`no_kandidat`'));
        
        return view('admin/hasil-suara',compact('title','data',));
    }
}
