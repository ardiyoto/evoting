<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Voting;
use App\Models\Kandidat;

class GrafikController extends Controller
{
    public function index(){
        $title = 'Hasil Suara';
        $data = Kandidat::All();
        $hasil = [];
        $kandidat = Kandidat::get();
        foreach ($kandidat as $key => $kan) {
            $id_kandidat = $kan->id_voting;
            $no_kandidat = $kan->no_kandidat;
            $total = voting::where('no_urut',$id_kandidat)->count();

            $a['name'] = $no_kandidat;
            $a['y'] = $total;
            array_push($hasil, $a);
        }

        return view('admin/grafik',compact('title','hasil'));
    }
    
    
}
