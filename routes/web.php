<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// DASHBOARD
Route::get('/admin/dashboard','DashboardController@index');
// END DASHBOARDs

// VOTE
Route::get('/', 'VoteController@login');
Route::post('/cek', 'VoteController@cekvoting');
Route::get('/voting', 'VoteController@vote');
Route::get('/save_voting/{nis}/{no_urut}', 'VoteController@save_voting');
Route::get('/thank', 'VoteController@thank');
Route::get('/endvoting', 'VoteController@endvoting');
// END VOTE

// KANDIDAT
Route::get('/admin/kandidat', 'KandidatController@index');
Route::get('admin/kandidat/form-kandidat/{id?}', 'KandidatController@form');
Route::post('/admin/kandidat/save', 'KandidatController@save');
Route::get('/admin/kandidat/delete/{id}', 'KandidatController@delete');
// END KANDIDAT

// PEMILIH
Route::get('/admin/pemilih', 'PemilihController@index');
Route::get('admin/pemilih/form-pemilih/{id?}', 'PemilihController@form');
Route::post('/admin/pemilih/save', 'PemilihController@save');
Route::get('/admin/pemilih/delete/{id}', 'PemilihController@delete');
Route::get('/admin/pemilih/token/{id}/{status}', 'PemilihController@set_token');
// END PEMILIH
Route::get('admin/grafik','GrafikController@index');
Route::get('admin/hasil-suara','HasilSuaraController@index');
Route::get('admin/hasil-suara/hasil','HasilSuaraController@hasil');
//Route::get('/admin/hasil-suara', function () {
//    return view('admin/hasil-suara');
//});

Route::get('/vote', function (){
    return view('vote');
});

Route::get('/admin/form-pemilih', function (){
    return view('admin/form-pemilih');
});


Route::get('admin/laporan', function(){
    return view('admin/laporan');
});

Route::get('admin/berita_acara','LaporanController@berita_acara');
Route::get('/absen','LaporanController@generate_absen');
