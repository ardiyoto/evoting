@extends('user.page')
@section('title', $title)
@section("time",20)
@section("url",$url)
@section('content')
    <form enctype="multipart/form-data" action="" method="post">
        @csrf
        <input type="hidden" name="_method" value="PUT" class="form-control">
        <div class="container">
            <div class="row align-middle vh-100">
                @foreach ($data as $vote)
                    <div class="col md-4 text-center my-2">
                        <h4 class="d-block rounded-circle bg-primary text-white" style="width:50px; height:50px; font-size: 25px;padding: 10px;margin: 0 auto;">{{ $vote->no_kandidat }}</h4>
                        <div class="thumb" style="padding: 25px 0;">
                            @if ($vote->foto)
                                <img src="{{ asset('assets/images/'.$vote->foto) }}" width="45%">
                            @else
                                <img src="{{ asset('assets/images/profile-user.png') }}" width="45%">
                            @endif
                        </div>
                        <h2 class="d-block pb-2 text-uppercase" style="color:#000;font-weight:600;">{{ $vote->nama_kandidat }}</h2>
                        <a class="btn btn-success" href="{{ url('save_voting/'.$nis."/".$vote->no_kandidat) }}">Voting</a>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#{{ "modal".$vote->no_kandidat }}">Profil</button>
                        <div class="modal fade" id="{{ "modal".$vote->no_kandidat }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ $vote->nama_kandidat }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h5>Visi</h5>
                                        <p>{{ $vote->visi }}</p>
                                        <h5>Misi</h5>
                                        <p>{{ $vote->misi }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </form>
@endsection