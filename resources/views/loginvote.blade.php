@extends('user.page')
@section('title', $title)
@section('content')
    @if (session('message'))
        <div class="alert alert-{{ session('type') }}">
            {{ session('message')}}
        </div>
    @endif
    <div id="welcome">
      <div class="row no-gutters min-vh-100">
          <div class="content col-md-6 d-flex align-items-center">
              <div class="inner">
                  <div class="logo">
                      <img src="{{ asset ('img/voting.png') }}" alt="">
                  </div>
                  <h1>SMP NEGERI 5 TIRTAYASA</h1>
                  <p>Sistem E-Voting Ketua OSIS</p>
              </div>
          </div>
          <div class="form col-md-6 d-flex align-items-center">
              <div class="inner">
                  <div class="card card-container login">
                    <img src="{{ asset ('img/kpuosis.png') }}" class="profile-img-card" id="profile-img">
                    <p id="profile-name" class="profile-name-card">SMP Negeri 5 Tirtayasa</p>
                    <form action="{{ url('/cek') }}" method="post" class="form-signin">
                      @csrf
                      <input class="form-control" type="text" name="nis_pemilih" id="nis_pemilih" class="form-control" placeholder="NIS" aria-describedby="NIS Pemilih" autofocus>
                      <input class="form-control" type="password" name="no_token" id="no_token" class="form-control" placeholder="NO. PIN" aria-describedby="No Token">
                      <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin">LOGIN</button>
                      <div class="foot">
                        <small>Masuk sebagai <a href="">Admin</a> ? </small>
                      </div>
                    </form>
                  </div>             
              </div>
          </div>
      </div>
  </div>
@endsection