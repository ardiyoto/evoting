<html>
    <head>
        <title>Berita Acara</title>
    </head>
<body>
    
    <style>
        @page { margin: 0 2%; }
        h5 { background: #000; padding: 10px 0; text-align: center; color: #fff; }
        table#data { border-collapse: collapse; border: 1px solid #000; }
        table#data th,table#data td { padding: 5px; border-collapse: collapse; border: 1px solid #000; }
        table#data th { background: #555; color: #fff; }
        .merah { background-color: #ff0000; }
        .merah td { color: #fff; }
    </style>
    <table width="100%">
        <tr>
            <td>
                <img src="{{ asset ('img/voting.png') }}" alt="" width="100">
            </td>
            <td>
            <h2  style="width:90%;text-align: center;">BERITA ACARA <br/> PEMILIHAN KETUA OSIS SMP TIRTAYASA <br/> PERIODE 2021-2023 </h2>
            <p>Minggu, 22 Juli 2021 | Ruang Kelas, TirtaYasa Lantai 10 <br/> Jl Sultan Agung Tirtayasa Serang Banten</p>
            <p>Hari Ini Minggu, Tanggal 22 Juli 2021, Panitia <br/> Pemilihan Ketua Osis Tirtayasi</p>
            
            </td>
        </tr>
    </table>
    <h5>Hasil Suara</h5>
    <p>Dikarenakan masa pengurusan ketua osis periode 2019/2020 telah berakhir maka dengan ini Pihak Pertama menyerahkan estafet kepemimpinan OSIS untuk kepengurusan periode 2019/2020 berdasarkan mekanisme dengan ketentuan yang berlaku peserta berkas pertanggung jawabannya</p>
    <p>Selanjutnya Pihak kedua menerima estafet kepengurusan OSIS tersebut dengan segala beban tugasnya yang akan dipertanggung jawabkan kepada seluruh anggota OSIS dan pihak sekolah sesuai ketentuan yang berlaku.</p>
    <!-- Data Pendaftar -->
    <table id="data" width="100%">
        <thead>
            <tr>
                <th>NO URUT</th>
                <th>NAMA KANDIDAT</th>
                <th>TOTAL SUARA</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $rsKandidat)
                <tr>
                    <td>{{ $rsKandidat->no_kandidat }}</td>
                    <td>{{ $rsKandidat->nama_kandidat }}</td>
                    <td>{{ $rsKandidat->hasil }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!-- End Data Pendaftar -->
    <p>Berita acara ini sah setelah hasil suara dikeluarkan oleh panitia.</p>
    <table width="100%">
        <tr>
            <td colspan="3" style="text-align: right">Serang , 12 Agustus 2021</td>
        </tr>
        @foreach ($data as $rsKandidat)
        <tr>
            <td width="5%">{{ $rsKandidat->no_kandidat }}.</td>
            <td>{{ $rsKandidat->nama_kandidat }}</td>
            <td style="height: 75px;">( .................................. )</td>
        </tr>
        @endforeach
    </table>
</body>
</html>