@extends('admin.template')
@section('title', $title)
@section('judul', $title)
@section('content')
  @if (session('message'))
  <div class="alert alert-{{ session('type') }}">
    {{ session('message')}}
  </div>
  @endif
  <a href="{{ url('admin/pemilih/form-pemilih')}}" class="btn btn-primary mb-3"> Input Pemilih </a>
  <div class="table-responsive">
    <table class="table table-striped dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
        <thead class="bg-primary text-light">
          <th>ID</th>
          <th>NIS</th>
          <th>NAMA</th>
          <th>KELAS</th>
          <th>JENIS KELAMIN</th>
          <th>STATUS</th>
          <th>NO. TOKEN</th>
          <th>ACTION</th>
        </thead>
        <tbody>
          @foreach ($data as $pem)
            <tr>
            <td scope="row">{{ $pem->id_pemilih }}</td>
              <td>{{ $pem->nis_pemilih}}</td>
              <td>{{ $pem->nama_pemilih}}</td>
              <td>{{ $pem->kelas_pemilih}}</td>
              <td>{{ $pem->jk_pemilih}}</td>
              <td>
                @php
                    $status = $pem->status == 1 ? 0 : 1;
                    $type = $pem->status == 1 ? 'danger' : 'success';
                @endphp
                {{-- <a href="{{ $pem->status == 1 ? '#' : url('/admin/pemilih/token/'.$pem->id_pemilih. '/'.$status) }}" class="btn btn-{{$type}} btn-sm">AKTIFKAN</a> --}}
                @if($pem->status==0)
                  @if($pem->no_token=="")
                    <a href="{{ url('/admin/pemilih/token/'.$pem->id_pemilih. '/'.$status) }}" class="btn btn-success btn-sm">AKTIFKAN</a>
                  @else
                    <span class="badge badge-pill bg-warning">Antri</span>
                  @endif
                @else
                  <span class="badge badge-pill bg-danger">Memilih</span>
                @endif
              </td>
              <td>{{ $pem->no_token}}</td>
              <td>
                <a href="{{ url('admin/pemilih/form-pemilih/'.$pem->id_pemilih)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                <a href="{{ url('admin/pemilih/delete/'.$pem->id_pemilih) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
  </div>
@endsection