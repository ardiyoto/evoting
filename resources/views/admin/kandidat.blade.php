@extends('admin.template')
@section('title', $title)
@section('judul', $title)
@section('content')
  @if (session('message'))
    <div class="alert alert-{{ session('type') }}">
      {{ session('message')}}
    </div>
  @endif
<a href="{{ url('admin/kandidat/form-kandidat')}}" class="btn btn-primary mb-3"> Input Kandidat </a>
<div class="table-responsive">
  <table class="table table-striped dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
      <thead class="bg-primary text-light">
        <th>ID</th>
        <th>FOTO</th>
        <th>NO. URUT</th>
        <th>NIS</th>
        <th>NAMA</th>
        {{-- <th>TEMPAT, TANGGAL LAHIR</th>
        <th>KELAS</th>
        <th>JENIS KELAMIN</th>
        <th>NO. TELP</th> --}}
        <th>ACTION</th>        
      </thead>
      <tbody>
        @foreach ($data as $kan)
          <tr>
            <td scope="row">{{ $kan->id_kandidat }}</td>
            <td>
                @if ($kan->foto)
                  <img src="{{ asset('assets/images/'.$kan->foto) }}" width="150" height="200">
                @else
                  <img src="{{ asset('assets/images/profile-user.png') }}" width="150" height="150">
                @endif
              </td>
              <td>{{ $kan->no_kandidat}}</td>
              <td>{{ $kan->nis_kandidat}}</td>
              <td>{{ $kan->nama_kandidat}}</td>
              {{-- <td>{{ $kan->tempat_lahir."," .date("d F Y", strtotime($kan->tanggal_lahir)) }}</td>
              <td>{{ $kan->kelas_kandidat}}</td>
              <td>{{ $kan->jk_kandidat}}</td>
              <td>{{ $kan->telp_kandidat}}</td> --}}
              <td>
                <a href="{{ url('admin/kandidat/form-kandidat/'.$kan->id_kandidat)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                <a href="{{ url('admin/kandidat/delete/'.$kan->id_kandidat) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
              </td>          
          </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection