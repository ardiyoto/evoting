<html>
    <head>
        <title>Report Absensi</title>
    </head>
<body>
    
    <style>
        @page { margin: 0 2%; }
        h5 { background: #000; padding: 10px 0; text-align: center; color: #fff; }
        table#data { border-collapse: collapse; border: 1px solid #000; }
        table#data th,table#data td { padding: 5px; border-collapse: collapse; border: 1px solid #000; }
        table#data th { background: #555; color: #fff; }
        .merah { background-color: #ff0000; }
        .merah td { color: #fff; }
    </style>
    <table width="100%">
        <tr>
            <td>
                <img src="{{ asset ('img/voting.png') }}" alt="" width="100">
            </td>
            <td style="width:90%;text-align: center;">
            <h2>Pemilihan Ketua Osis</h2>
                <p>Jl Sultan Agung Tirtayasa Kec. Tirtayasa Kab. Serang Banten  <br/>www.smptirtayasa.sch.id , Email : mail@tirtayasa.com, Telp : (0351) 5115099</p>
            </td>
        </tr>
    </table>
    <h5>ABSENSI PEMILIHAN KETUA OSIS</h5>
    <!-- Data Pendaftar -->
    <table id="data" width="100%">
        <thead>
            <tr>
                <th width="8%">NO</th>
                <th width="12%">NIS</th>
                <th width="50%">NAMA</th>
                <th width="10%">KELAS</th>
                <th width="20%" colspan="2">TTD</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $rsPemilih)
                
            
                <tr>
                    <td width="5%">{{ $loop->iteration }}</td>
                    <td width="15%">{{ $rsPemilih->nis_pemilih }}</td>
                    <td>{{ $rsPemilih->nama_pemilih }}</td>
                    <td>{{ $rsPemilih->kelas_pemilih }}</td>
                    <td>{{ $loop->iteration%2 > 0 ? $loop->iteration."." : "" }}</td>
                    <td>{{ $loop->iteration%2 == 0 ? $loop->iteration."." : "" }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!-- End Data Pendaftar -->
    
</body>
</html>