@extends('admin.template')
@section('title', $title)
@section('judul', $title)
@section('content')
    @if (session('message'))
        <div class="alert alert-{{ session('type') }}">
            {{ session('message')}}
        </div>
    @endif
      <form action="{{ url('/admin/pemilih/save') }}" method="POST">
        @csrf
        <div class="form-group">
          <label for="nis_pemilih">NIS</label>
          <input type="hidden" name="id_pemilih" value="{{ @$data->id_pemilih }}">
           <input type="text" name="nis_pemilih" id="nis_pemilih" class="form-control" placeholder="" aria-describedby="NIS Pemilih"  @error('nis_pemilih') is-invalid @enderror value="{{ old('nis_pemilih') ? old('nis_pemilih') : @$data->nis_pemilih }}">
           @error('nis_pemilih')
              <p class="invalid-feedback">{{ $errors->first('nis_pemilih') }}</p>
           @enderror
        </div>
      <div class="form-group">
      <label for="kelas_pemilih">Nama</label>
        <input type="text" name="nama_pemilih" id="nama_pemilih" class="form-control" placeholder="" aria-describedby="Nama Pemilih"  @error ('nama_pemilih') is-invalid @enderror value="{{ old('nama_pemilih') ? old('nama_pemilih') : @$data->nama_pemilih }}" required>
        @error('nama_pemilih')
          <p class="invalid-feedback">{{ $errors->first('nama_pemilih') }}</p>
        @enderror
      </div>
      <div class="form-group">
        <label for="kelas_pemilih">Kelas</label>
        <input type="text" name="kelas_pemilih" id="kelas_pemilih" class="form-control" placeholder="" aria-describedby="Kelas Pemilih" @error ('kelas_pemilih') is-invalid @enderror value="{{ old('kelas_pemilih') ? old('kelas_pemilih') : @$data->kelas_pemilih }}" required>
        @error('kelas_pemilih')
          <p class="invalid-feedback">{{ $errors->first('kelas_pemilih')}}</p>
        @enderror 
      </div>
      <div class="form-group">
        <label for="@jk_pemilih">Jenis Kelamin :</label> <br>
        <div class="form-check form-check-inline">
          <label for="@jk_pemilih">
            <input type="radio" name="jk_pemilih" value="L" {{@$data->jk_pemilih == 'L'? 'checked' : ''}} > Laki-Laki <br>
            <input type="radio" name="jk_pemilih" value="P" {{@$data->jk_pemilih == 'P'? 'checked' : ''}} > Perempuan
          </label>
        </div>
      </div>
      <button type="submit" class="btn btn-outline-primary">Simpan</button>
    </form>
@endsection