@extends('admin.template')
@section('title', 'Hasil Suara')
@section('judul', 'Hasil Suara')
@section('content')
<div class="table-responsive">
  <table class="table table-striped dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
      <thead class="bg-primary text-light">
        <th>ID</th>
        <th>FOTO</th>
        <th>NO. URUT</th>
        <th>NIS KANDIDAT</th>
        <th>NAMA KANDIDAT</th>
        <th>HASIL SUARA</th>
      </thead>
      <tbody>
        @foreach ($data as $kan)
          <tr>
            <td scope="row">{{ $kan->id_kandidat }}</td>
            <td>
                @if ($kan->foto)
                  <img src="{{ asset('assets/images/'.$kan->foto) }}" width="150" height="200">
                @else
                  <img src="{{ asset('assets/images/profile-user.png') }}" width="150" height="150">
                @endif
              </td>
              <td>{{ $kan->no_kandidat}}</td>
              <td>{{ $kan->nis_kandidat}}</td>
              <td>{{ $kan->nama_kandidat}}</td>
              <td>{{ $kan->hasil }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection