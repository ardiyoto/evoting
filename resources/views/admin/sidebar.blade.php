        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <i class="fas fa-poll"></i>
                <div class="sidebar-brand-text mx-3">E-VOTING</div>
            </a>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/dashboard')}}">
                    <i class="fas fa-home"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Nav Item - Input Kandidat -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/kandidat')}}">
                    <i class="fas fa-download"></i>
                    <span>Kandidat</span></a>
            </li>

            <!-- Nav Item - Upload DPT -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/pemilih')}}">
                    <i class="fas fa-upload"></i>
                    <span>Pemilih</span></a>
            </li>

            <!-- Nav Item - Hasil Suara -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/hasil-suara')}}">
                    <i class="fas fa-vote-yea"></i>
                    <span>Hasil Suara</span></a>
            </li>

            <!-- Nav Item - Laporan -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-copy"></i>
                    Laporan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ url('/absen')}}" target="_blank">Absensi</a>
                    <a class="dropdown-item" href="{{ url('admin/berita_acara')}}" target="_blank">Berita Acara</a>
                </div>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/rpt-laporan')}}">
                    <i class="fas fa-copy"></i>
                    <span>Laporan</span></a>
            </li> --}}


            <!-- Nav Item - Log Out -->
            <li class="nav-item">
                <a class="nav-link" href="tables.html">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Log Out</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->