@extends('admin.template')
@section('title', $title)
@section('judul', $title)
@section('content')
  @if (session('message'))
    <div class="alert alert-{{ session('type') }}">
      {{ session('message')}}
    </div>
  @endif

    <form action="{{ url('admin/kandidat/save') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="no_kandidat">No. Kandidat</label>
            <input type="hidden" name="id_kandidat" value="{{ @$data->id_kandidat }}">
            <input type="text" name="no_kandidat" id="no_kandidat" class="form-control" placeholder="" aria-describedby="No Kandidat" @error('no_kandidat') is-invalid @enderror value="{{ old('no_kandidat') ? old('no_kandidat') : @$data->no_kandidat }}">
            @error('no_kandidat')
              <p class="invalid-feedback">{{ $errors->first('no_kandidat') }}</p>
            @enderror
          </div>
        </div>
        <div class="form-group col-md-8">
          <label for="nis_kandidat">NIS</label>
          <input type="text" name="nis_kandidat" id="nis_kandidat" class="form-control" placeholder="" aria-describedby="NIS Kandidat" @error ('nis_kandidat') is-invalid @enderror value="{{ old('nis_kandidat') ? old('nis_kandidat') : @$data->nis_kandidat }}">
          @error('nis_kandidat')
            <p class="invalid-feedback">{{ $errors->first('nis_kandidat') }}</p>
          @enderror
        </div>
      </div>
      <div class="form-group">
        <label for="nama_kandidat">Nama</label>
        <input type="text" name="nama_kandidat" id="nama_kandidat" class="form-control" placeholder="" aria-describedby="Nama Kandidat" @error ('nama_kandidat') is-invalid @enderror value="{{ old('nama_kandidat') ? old('nama_kandidat') : @$data->nama_kandidat }}">
        @error('nama_kandidat')
          <p class="invalid-feedback">{{ $errors->first('nama_kandidat') }}</p>
        @enderror
      </div>
      <div class="form-group">
        <label for="nama">Tempat, Tanggal Lahir *</label>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" aria-describedby="Tempat Lahir" @error ('tempat_lahir') is-invalid @enderror value="{{ old('tempat_lahir') ? old('tempat_lahir') : @$data->tempat_lahir }}">
                @error('tempat_lahir')
                  <p class="invalid-feedback">{{ $errors->first('tempat_lahir')}}</p>
                @enderror 
            </div>
            <div class="col-md-6">
                <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" aria-describedby="Tanggal Lahir" @error ('tanggal_lahir') is-invalid @enderror value="{{ old('tanggal_lahir') ? old('tanggal_lahir') : @$data->tanggal_lahir }}" required>
            </div>                      
        </div>
      </div>
      <div class="form-group">
        <label for="kelas_kandidat">Kelas</label>
        <input type="text" name="kelas_kandidat" id="kelas_kandidat" class="form-control" placeholder="" aria-describedby="Kelas Kandidat" @error ('kelas_kandidat') is-invalid @enderror value="{{ old('kelas_kandidat') ? old('kelas_kandidat') : @$data->kelas_kandidat }}">
        @error('kelas_kandidat')
          <p class="invalid-feedback">{{ $errors->first('kelas_kandidat')}}</p>
        @enderror 
      </div>
      <div class="form-group">
        <label for="@jk_kandidat">Jenis Kelamin :</label> <br>
        <div class="form-check form-check-inline">
          <label for="@jk_kandidat">
            <input type="radio" name="jk_kandidat" value="L" {{@$data->jk_kandidat == 'L'? 'checked' : ''}} > Laki-Laki <br>
            <input type="radio" name="jk_kandidat" value="P" {{@$data->jk_kandidat == 'P'? 'checked' : ''}} > Perempuan
          </label>
        </div>
      </div>
      <div class="form-group">
        <label for="telp_kandidat">No. Handphone / WA *</label>
        <input type="text" name="telp_kandidat" id="telp_kandidat" class="form-control" placeholder="" aria-describedby="Telp Kandidat" @error ('telp_kandidat') is-invalid @enderror value="{{ old('telp_kandidat') ? old('telp_kandidat') : @$data->telp_kandidat }}">
        @error('telp_kandidat')
          <p class="invalid-feedback">{{ $errors->first('tempat_lahir')}}</p>
        @enderror
      </div>
      <div class="form-group">
        <label for="alamat_kandidat">Alamat</label>
        <textarea class="form-control" name="alamat_kandidat" id="alamat_kandidat" rows="3" @error ('alamat_kandidat') is-invalid @enderror>{{ old('alamat_kandidat') ? old('alamat_kandidat') : @$data->alamat_kandidat }}</textarea>
      </div>
      <div class="form-group">
        <label for="pengalaman_kandidat">Pengalaman Berorganisasi</label>
        <textarea class="form-control" name="pengalaman_kandidat" id="pengalaman_kandidat" rows="3">{{ old('pengalaman_kandidat') ? old('pengalaman_kandidat') : @$data->pengalaman_kandidat }}</textarea>
      </div>
      <div class="form-group">
        <label for="visi">Visi</label>
        <textarea class="form-control" name="visi" id="visi" rows="3" @error ('visi') is-invalid @enderror>{{ old('visi') ? old('visi') : @$data->visi }}</textarea>
      </div>
      <div class="form-group">
        <label for="misi">Misi</label>
        <textarea class="form-control" name="misi" id="misi" rows="3" @error ('misi') is-invalid @enderror>{{ old('misi') ? old('misi') : @$data->misi }}</textarea>
      </div>
      <div class="form-group">
        <label for="foto">Masukkan Foto : </label>
        <input type="hidden" name="id_kandidat" value="{{ @$data->id_kandidat }}">
        <input type="hidden" name="old_foto" value="{{ @$data->foto }}">
        <input type="file" class="form-control-file" name="foto" id="foto">
      </div>
      <button type="submit" class="btn btn-outline-primary">SIMPAN</button>
    </form>
@endsection