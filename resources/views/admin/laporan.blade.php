@extends('admin.template')
@section('title', 'Laporan')
@section('judul', 'Laporan')
@section('content')
<div class="row no-gutters">
    <div class="main-content col-md-10 ml-auto">
        <div class="title">
            <h2>laporan</h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2>Export Excel</h2>
                <a class="btn btn-warning mt-3 d-block" href="">Export Semua Data</a>
            </div>
            <div class="col-md-6">
                <h2>Export PDF</h2>
                <a class="btn btn-warning mt-3 d-block" target="blank" href="">Hasil Suara</a>
            </div>                                           
        </div>
    </div>
</div>
@endsection