@extends('user.page')
@section("time",5)
@section("url",$url)
@section("title",$title)

@section('content')
  <div class="card nothing">
    <div class="card-body text-center d-flex" style="height:98vh;flex-direction: column;justify-content:center;align-item:center;">
      <img src="{{ asset ('img/cek.png') }}" style="width:150px; height:150px; margin: 0 auto;">
      <h1 class="card-title" style="width:100%">Terima Kasih !</h1>
      <p class="card-text" style="width:100%;font-size: 20px;">Atas partisipasi Anda dalam melakukan kegiatan E-Voting ini!</p>
    </div>
  </div>
@endsection